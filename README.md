MAVProxy

This is a MAVLink ground station written in python. 

Please see http://ardupilot.github.io/MAVProxy/ for more information

This ground station was developed as part of the CanberraUAV OBC team
entry

Setup Guide
-------
The pip-installed MAVProxy will need to uninstalled (if already installed) to prevent system
conflicts:
````
sudo apt-get install git
pip uninstall MAVProxy
````
you might need to add "sudo" in front of "pip" if you receive an error regarding insufficient priviledges.

Use git to download the MAVProxy source:
````
git clone https://bitbucket.org/utiasfsc/mavproxy.git
````

After making the desired changes, MAVProxy is required to be installed (any change to the
modules won't work otherwise). This needs to happen a�er any changes to the source code. This
can be done by:
````
python setup.py build install --user
````

MAVProxy can then be run as per normal.
````
mavproxy.py
````

License
-------

MAVProxy is released under the GNU General Public License v3 or later

[![Gitter](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/ArduPilot/MAVProxy?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
